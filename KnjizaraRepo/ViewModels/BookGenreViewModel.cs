﻿using KnjizaraRepo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.ViewModels
{
    public class BookGenreViewModel
    {
        public Book Book { get; set; }
        public IEnumerable<Genre> Genres { get; set; }
        public int SelectedGenreId { get; set; }

    }
}