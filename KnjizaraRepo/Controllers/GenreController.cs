﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository;
using KnjizaraRepo.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnjizaraRepo.Controllers
{
    public class GenreController : Controller
    {
        private IGenreRepository genreRepo = new GenreRepo();

        // GET: Genre/Index
        public ActionResult Index()
        {
            var genres = genreRepo.GetAll();

            return View(genres);
        }


        // GET: Genre/Create
        public ActionResult Create()
        {
            return View();
        }

        //POST: Genre/Create
        [HttpPost]
        public ActionResult Create(Genre genre)
        {
            if (genreRepo.Create(genre))
            {
                return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih žanrova
            }
            return View("Error");
        }

        // GET: ProductCategory/Edit/5
        public ActionResult Edit(int id)
        {
            var genre = genreRepo.GetById(id);

            return View(genre);
        }

        // POST: ProductCategory/Edit/5
        [HttpPost]
        public ActionResult Edit(Genre retgenre)
        {
            try
            {
                int exisingId = retgenre.Id;
                string newName = retgenre.Name;

                var updatedGenre = new Genre()
                {
                    Id = exisingId,
                    Name = newName
                };

                genreRepo.Update(updatedGenre);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }


        // GET: Genre/Delete
        public ActionResult Delete(int id)
        {
            try
            {
                genreRepo.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Genre/Deleted
        public ActionResult Deleted()
        {
            var genres = genreRepo.GetAll();

            return View(genres);
        }


    }
}