﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository;
using KnjizaraRepo.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnjizaraRepo.Controllers
{
    public class BookstoreController : Controller
    {
        private IBookstoreRepository bookstoreRepo = new BookstoreRepo();

        // GET: ProductCategory
        public ActionResult Index()
        {
            var bookstores = bookstoreRepo.GetAll();

            return View(bookstores);
        }


        // GET: ProductCategory/Create
        public ActionResult Create()
        {
            return View();
        }

        //POST: Bookstore/Create
       [HttpPost]
        public ActionResult Create(Bookstore bookstore)
        {
            if (bookstoreRepo.Create(bookstore))
            {
                return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih knjizara
            }
            return View("Error");
        }

        // GET: Bookstore/Books
        public ActionResult Books(int id)
        {
            BookController.Bs = id;

            return RedirectToAction("Index", "Book");
        }


    }
}