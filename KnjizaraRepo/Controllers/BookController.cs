﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository;
using KnjizaraRepo.Repository.Interfaces;
using KnjizaraRepo.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KnjizaraRepo.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository bookRepo = new BookRepo();
        private IGenreRepository genrebookRepo = new GenreRepo();
        public static int Bs = 0;

        // GET: Book
        public ActionResult Index(string sortby)
        {
            switch (sortby)
            {
                case "NameUp":
                    var AllBooksOrderedByNameUp = bookRepo.GetAll(Bs).OrderBy(w => w.Name);
                    return View(AllBooksOrderedByNameUp);

                case "NameDown":
                    var AllBooksOrderedByNameDown = bookRepo.GetAll(Bs).OrderByDescending(w => w.Name);
                    return View(AllBooksOrderedByNameDown);

                case "PriceUp":
                    var AllBooksOrderedByPriceUp = bookRepo.GetAll(Bs).OrderBy(w => w.Price);
                    return View(AllBooksOrderedByPriceUp);

                case "PriceDown":
                    var AllBooksOrderedByPriceDown = bookRepo.GetAll(Bs).OrderByDescending(w => w.Price);
                    return View(AllBooksOrderedByPriceDown);

                default:
                    return View(bookRepo.GetAll(Bs));
            }
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View(genrebookRepo.GetAll());
        }

        //POST: Book/Create
        [HttpPost]
        public ActionResult Create(string NameOfBook, double PriceOfBook, int IdGenreOfBook)
        {
            Book book = new Book() { Name = NameOfBook, Price = PriceOfBook, Bookgenre = genrebookRepo.GetById(IdGenreOfBook)};

            if (bookRepo.Create(Bs, book))
            {
                return RedirectToAction("Index");   // nakon uspesnog dodavanja daj listing svih knjiga
            }
            return View("Error");
        }


        // GET: Book/Edit
        public ActionResult Edit(int id)
        {
            BookGenreViewModel vm = new BookGenreViewModel();
            vm.Book = bookRepo.GetById(id);
            vm.Genres = genrebookRepo.GetAll();

            return View(vm);
        }

        // POST: Book/Edit
        [HttpPost]
        public ActionResult Edit(BookGenreViewModel vm)
        {
            try
            {
                Genre newGenre = new Genre();
                newGenre = genrebookRepo.GetById(vm.SelectedGenreId);
                Book updatedBook = new Book();
                updatedBook = vm.Book;
                updatedBook.Bookgenre = newGenre;

                bookRepo.Update(updatedBook);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }


        // GET: Book/Delete
        public ActionResult Delete(int id)
        {
            try
            {
                bookRepo.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        // GET: Book/Deleted
        public ActionResult Deleted()
        {
            var books = bookRepo.GetAll(Bs);

            return View(books);
        }
    }
}