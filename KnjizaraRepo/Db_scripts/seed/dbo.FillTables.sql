﻿--punjnje tabela
INSERT INTO dbo.AllGenres (GenreName, GenreDeleted) VALUES ('Science', 0);
INSERT INTO dbo.AllGenres (GenreName, GenreDeleted) VALUES ('Comedy', 0);
INSERT INTO dbo.AllGenres (GenreName, GenreDeleted) VALUES ('Horror', 0);

INSERT INTO dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) VALUES ('Prva Knjiga', 150.8, 1, 0);
INSERT INTO dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) VALUES ('Druga Knjiga', 240.2, 2, 0);
INSERT INTO dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) VALUES ('Treca Knjiga', 170.9, 3, 0);
INSERT INTO dbo.AllBooks (BookName, BookPrice, BookGenreId, BookDeleted) VALUES ('Cetvrta Knjiga', 152.1, 2, 0);

INSERT INTO dbo.AllBookstores (BookstoreName) VALUES ('Knjizara 1');
INSERT INTO dbo.AllBookstores (BookstoreName) VALUES ('Knjizara 2');

INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (1, 1);
INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (1, 2);
INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (1, 3);
INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (1, 4);
INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (2, 2);
INSERT INTO dbo.BooksInBookstores (BookstoreId, BookId) VALUES (2, 4);     

-- select * from AllBooks;  select * from AllBookstores; select * from AllGenres; select * from BooksInBookstores;  delete from AllBookstores where BookstoreId = 3; INSERT INTO AllBookstores (BookstoreName) VALUES ('Knjizara 5')
-- SELECT * FROM AllBookstores bs JOIN BooksInBookstores bis ON bs.BookstoreId = bis.BookstoreId LEFT JOIN AllBooks b ON bis.BookId = b.BookId WHERE bs.BookstoreId = 1
-- SELECT * FROM AllBooks b, AllGenres g where b.BookGenreId = g.GenreId;  

-- SELECT * FROM BooksInBookstores bib LEFT JOIN AllBooks b ON bib.BookId = b.BookId LEFT JOIN AllGenres g ON b.BookGenreId = g.GenreId WHERE BookstoreId = 2


