﻿--brisanje ranijih tabela ako postoje

DROP TABLE IF EXISTS [dbo].[BooksInBookstores];
DROP TABLE IF EXISTS [dbo].[AllBookstores];
DROP TABLE IF EXISTS [dbo].[AllBooks];
DROP TABLE IF EXISTS [dbo].[AllGenres];

--inicijalizacija tabela

CREATE TABLE [dbo].[AllGenres]
(
	[GenreId] INT IDENTITY(1,1) PRIMARY KEY,
	[GenreName] NVARCHAR(20),
	[GenreDeleted] BIT,
);

CREATE TABLE [dbo].[AllBooks]
(
	[BookId] INT IDENTITY(1,1) PRIMARY KEY,
	[BookName] NVARCHAR(20),
	[BookPrice] FLOAT,
	[BookGenreId] INT, 
	[BookDeleted] BIT,
	FOREIGN KEY (BookGenreId) REFERENCES dbo.AllGenres(GenreId) ON DELETE CASCADE
);

CREATE TABLE [dbo].[AllBookstores]
(
	[BookstoreId] INT IDENTITY(1,1) PRIMARY KEY,
	[BookstoreName] NVARCHAR(20)
);

CREATE TABLE [dbo].[BooksInBookstores]
(
	[BookstoreId] INT,
	[BookId] INT,
	PRIMARY KEY([BookstoreId], [BookId]),
	FOREIGN KEY (BookstoreId) REFERENCES dbo.AllBookstores(BookstoreId) ON DELETE CASCADE,
	FOREIGN KEY (BookId) REFERENCES dbo.AllBooks(BookId) ON DELETE CASCADE
);

