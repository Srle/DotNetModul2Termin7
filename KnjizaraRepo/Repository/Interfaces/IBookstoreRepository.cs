﻿using KnjizaraRepo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.Repository.Interfaces
{
    public interface IBookstoreRepository
    {
        IEnumerable<Bookstore> GetAll();
        Bookstore GetById(int id);
        bool Create(Bookstore bookstore);
        void Update(Bookstore bookstore);
        void Delete(int id);
    }
}

