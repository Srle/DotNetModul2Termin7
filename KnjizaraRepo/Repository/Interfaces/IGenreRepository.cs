﻿using KnjizaraRepo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.Repository.Interfaces
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        bool Create(Genre genre);
        void Update(Genre genre);
        void Delete(int id);
    }
}