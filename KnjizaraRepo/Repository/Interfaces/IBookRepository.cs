﻿using KnjizaraRepo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.Repository.Interfaces
{
    public interface IBookRepository
    {
        IEnumerable<Book> GetAll(int bs);
        Book GetById(int id);
        bool Create(int bs, Book book);
        void Update(Book book);
        void Delete(int id);
    }
}