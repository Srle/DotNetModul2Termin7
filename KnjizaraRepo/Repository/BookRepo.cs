﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using KnjizaraRepo.Controllers;

namespace KnjizaraRepo.Repository
{
    public class BookRepo : IBookRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        private IGenreRepository genrebookRepo1 = new GenreRepo();

        public bool Create(int bs, Book book)
        {
            try
            {
                string query = "INSERT INTO AllBooks (BookName, BookPrice, BookDeleted, BookGenreId) VALUES (@bookName, @bookPrice ,'false', @bookGenreId);";
                query += " SELECT SCOPE_IDENTITY();";    // selektuj id novododatog zapisa nakon upisa u bazu
                string query1= " INSERT INTO BooksInBookstores (BookstoreId, BookId) VALUES (@bs, @BookId)";


                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    int bookID;
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@bookName", book.Name);
                    cmd.Parameters.AddWithValue("@bookPrice", book.Price);
                    cmd.Parameters.AddWithValue("@bookGenreId", book.Bookgenre.Id);
                    // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    bookID = int.Parse(newFormedId.ToString());
                    con.Close();

                    cmd.CommandText = query1;
                    cmd.Parameters.AddWithValue("@bs", bs);
                    cmd.Parameters.AddWithValue("@BookId", bookID);
                    // stitimo od SQL Injection napada

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();                                        // zatvori konekciju

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa nove knjige. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }

        }

        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE AllBooks SET BookDeleted = 1 WHERE BookId = @bookId;";

                Connection();   // inicijaizuj novu konekciju  

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@bookId", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja knjige. {ex.StackTrace}");
                throw ex;
            }

        }


        public IEnumerable<Book> GetAll(int bs)
        {
            List<Book> books = new List<Book>();

            try
            {
                string query = "SELECT * FROM BooksInBookstores bib LEFT JOIN AllBooks b ON bib.BookId = b.BookId LEFT JOIN AllGenres g ON b.BookGenreId = g.GenreId WHERE BookstoreId = @bs";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@bs", bs);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllBooks"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["AllBooks"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookName = dataRow["BookName"].ToString();
                    double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                    bool bookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    int bookgenreId = int.Parse(dataRow["BookGenreId"].ToString());

                    books.Add(new Book() { Id = bookId, Name = bookName, Price = bookPrice, Deleted = bookDeleted, Bookgenre = genrebookRepo1.GetById(bookgenreId) });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjige. {ex.StackTrace}");
                throw ex;
            }

            return books;
        }


        public Book GetById(int id)
        {
            Book book = null;

            try
            {
                string query = "SELECT * FROM AllBooks b WHERE b.BookId = @BookId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@BookId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllBooks"); // 'AllBookstore' je naziv tabele u dataset-u
                    dt = ds.Tables["AllBooks"];    // formiraj DataTable na osnovu AllBookstore tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int bookId = int.Parse(dataRow["BookId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookName = dataRow["BookName"].ToString();
                    double bookPrice = double.Parse(dataRow["BookPrice"].ToString());
                    bool bookDeleted = bool.Parse(dataRow["BookDeleted"].ToString());
                    int bookgenreId = int.Parse(dataRow["BookGenreId"].ToString());

                    book = new Book() { Id = bookId, Name = bookName, Price = bookPrice, Deleted = bookDeleted, Bookgenre = genrebookRepo1.GetById(bookgenreId) };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjige sa id-om: {book.Id}. {ex.StackTrace}");
                throw ex;
            }

            return book;
        }


        public void Update(Book book)
        {
            try
            {
                string query = "UPDATE AllBooks SET BookName = @bookName, BookPrice = @bookPrice, BookGenreId = @bookGenreId  WHERE BookId = @bookId;";

                Connection();   // inicijaizuj novu konekciju string 

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@BookId", book.Id);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@BookName", book.Name);
                    cmd.Parameters.AddWithValue("@bookPrice", book.Price);
                    cmd.Parameters.AddWithValue("@bookGenreId", book.Bookgenre.Id);

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom editovanja knjige. {ex.StackTrace}");
                throw ex;
            }
        }



    }
}