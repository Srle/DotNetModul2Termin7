﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.Repository
{
    public class GenreRepo : IGenreRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }


        public bool Create(Genre genre)
        {
            try
            {
                string query = "INSERT INTO AllGenres (GenreName, GenreDeleted) VALUES (@GenreName, 0);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa novog žanra. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }

        }

        public void Delete(int id)
        {
            try
            {
                string query = "UPDATE AllGenres SET GenreDeleted = 1 WHERE GenreId = @GenreId;";

                Connection();   // inicijaizuj novu konekciju 

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@GenreId", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja žanra. {ex.StackTrace}");
                throw ex;
            }

        }


        public IEnumerable<Genre> GetAll()
        {
            List<Genre> genres = new List<Genre>();
            try
            {
                string query = "SELECT * FROM AllGenres;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllGenres"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["AllGenres"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }



                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele 
                {
                    int genreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string genreName = dataRow["GenreName"].ToString();
                    bool genreDeleted = bool.Parse(dataRow["GenreDeleted"].ToString());
                    
                    //if (genredeleted == 1) genreDeleted = true;
                    
                    genres.Add(new Genre() { Id = genreId, Name = genreName, Deleted = genreDeleted});
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja žanrova. {ex.StackTrace}");
                throw ex;
            }

            return genres;
        }


        public Genre GetById(int id)
        {
            Genre genre = null;

            try
            {
                string query = "SELECT * FROM AllGenres ge WHERE ge.GenreId = @GenreId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@GenreId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllGenres"); // 'AllGenres' je naziv tabele u dataset-u
                    dt = ds.Tables["AllGenres"];    // formiraj DataTable na osnovu AllGenres tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int genreId = int.Parse(dataRow["GenreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string genreName = dataRow["GenreName"].ToString();

                    genre = new Genre() { Id = genreId, Name = genreName };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjige sa id-om: {genre.Id}. {ex.StackTrace}");
                throw ex;
            }

            return genre;
        }


        public void Update(Genre genre)
        {
            try
            {
                string query = "UPDATE AllGenres SET GenreName = @GenreName WHERE GenreId = @GenreId;";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@GenreId", genre.Id);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@GenreName", genre.Name);

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom editovanja kžanra. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}