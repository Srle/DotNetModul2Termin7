﻿using KnjizaraRepo.Models;
using KnjizaraRepo.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace KnjizaraRepo.Repository
{
    public class BookstoreRepo : IBookstoreRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }


        public bool Create(Bookstore bookstore)
        {
            try
            {
                string query = "INSERT INTO AllBookstores (BookstoreName) VALUES (@BookstoreName);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@BookstoreName", bookstore.Name);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju

                    if (newFormedId != null)
                    {
                        return true;    // upis uspesan, generisan novi id
                    }
                }
                return false;   // upis bezuspesan
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska pilikom upisa nove knjizare. " + ex.StackTrace);  // ispis u output-prozorcicu
                throw ex;
            }

        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM AllBookstores WHERE BookstoreId = @BookstoreId;";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@bookstoreId", id);  // stitimo od SQL Injection napada

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja knjizare. {ex.StackTrace}");
                throw ex;
            }

        }


        public IEnumerable<Bookstore> GetAll()
        {
            List<Bookstore> bookstores = new List<Bookstore>();

            try
            {
                string query = "SELECT * FROM AllBookstores;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllBookstores"); // 'ProductCategory' je naziv tabele u dataset-u
                    dt = ds.Tables["AllBookstores"];    // formiraj DataTable na osnovu ProductCategory tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }

                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int bookstoreId = int.Parse(dataRow["BookstoreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookstoreName = dataRow["BookstoreName"].ToString();

                    bookstores.Add(new Bookstore() { Id = bookstoreId, Name = bookstoreName });
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom izlistavanja knjizare. {ex.StackTrace}");
                throw ex;
            }

            return bookstores;
        }


        public Bookstore GetById(int id)
        {
            Bookstore bookstore = null;

            try
            {
                string query = "SELECT * FROM AllBookstores bs WHERE bs.BookstoreId = @bookstoreId;";
                Connection();   // inicijaizuj novu konekciju

                DataTable dt = new DataTable(); // objekti u 
                DataSet ds = new DataSet();     // koje smestam podatke

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@bookstoreId", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter(); // bitan objekat pomocu koga preuzimamo podatke i izvrsavamo upit
                    dadapter.SelectCommand = cmd;                   // nakon izvrsenog upita

                    // Fill(...) metoda je bitna, jer se prilikom poziva te metode izvrsava upit nad bazom podataka
                    dadapter.Fill(ds, "AllBookstores"); // 'AllBookstore' je naziv tabele u dataset-u
                    dt = ds.Tables["AllBookstores"];    // formiraj DataTable na osnovu AllBookstore tabele u DataSet-u
                    con.Close();                  // zatvori konekciju
                }


                foreach (DataRow dataRow in dt.Rows)            // izvuci podatke iz svakog reda tj. zapisa tabele
                {
                    int bookstoreId = int.Parse(dataRow["BookstoreId"].ToString());    // iz svake kolone datog reda izvuci vrednost
                    string bookstoreName = dataRow["BookstoreName"].ToString();

                    bookstore = new Bookstore() { Id = bookstoreId, Name = bookstoreName };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja knjizare sa id-om: {bookstore.Id}. {ex.StackTrace}");
                throw ex;
            }

            return bookstore;
        }


        public void Update(Bookstore bookstore)
        {
            try
            {
                string query = "UPDATE AllBookstores SET BookstoreName = @bookstoreName WHERE BookstoreId = @bookstoreId;";

                Connection();   // inicijaizuj novu konekciju

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;    // ovde dodeljujemo upit koji ce se izvrsiti nad bazom podataka
                    cmd.Parameters.AddWithValue("@BookstoreId", bookstore.Id);  // stitimo od SQL Injection napada
                    cmd.Parameters.AddWithValue("@BookstoreName", bookstore.Name);

                    con.Open();                                         // otvori konekciju
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();                                        // zatvori konekciju
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom editovanja knjizare. {ex.StackTrace}");
                throw ex;
            }
        }




























    }
}