﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KnjizaraRepo.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Genre Bookgenre { get; set; }
        public bool Deleted { get; set; }
        

        public Book()
        {
        }

        public Book(int id, string name, double price, bool deleted)
        {
            Bookgenre = new Genre();
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Deleted = deleted;
        }

        public Book(int id, string name, double price, Genre genre, bool deleted)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Bookgenre = genre;
            this.Deleted = deleted;
        }

    }
}