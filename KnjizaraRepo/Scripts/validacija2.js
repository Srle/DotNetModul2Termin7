﻿window.onload = function () {
    var name = document.querySelector("input[name='Book.Name']");

    var price = document.querySelector("input[name='Book.Price']");

    name.onfocus = function () {
        let nameSpan2 = document.querySelector("#nameSpan2");
        nameSpan2.innerText = "";
    };

    price.onfocus = function () {
        let priceSpan2 = document.querySelector("#priceSpan2");
        priceSpan2.innerText = "";
    };
};
var ret1, ret2;

function posalji2(form) {
    var name = form["Book.Name"].value;
    var price = form["Book.Price"].value;

    if (!name) {
        let nameSpan2 = document.querySelector("#nameSpan2");
        nameSpan2.innerText = "polje ne sme biti prazno";
        ret1 = false;
    }

    else if (name.length >= 10) {
        let nameSpan2 = document.querySelector("#nameSpan2");
        nameSpan2.innerText = "dužina imena ne sme preći 10 karaktera";
        ret1 = false;
    }
    else { ret1 = true; }

    if (!price) {
        let priceSpan2 = document.querySelector("#priceSpan2");
        priceSpan2.innerText = "polje ne sme biti prazno";
        ret2 = false;
    }

    else if (isNaN(price)) {
        let priceSpan2 = document.querySelector("#priceSpan2");
        priceSpan2.innerText = "polje mora biti broj";
        ret2 = false;
    }

    else if (price < 0) {
        let priceSpan2 = document.querySelector("#priceSpan2");
        priceSpan2.innerText = "broj mora biti veci od nule";
        ret2 = false;
    }
    else { ret2 = true; }

    return ret1 && ret2;
}
