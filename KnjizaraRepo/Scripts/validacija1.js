﻿window.onload = function () {
    var name = document.querySelector("input[name='NameOfBook']");

    var price = document.querySelector("input[name='PriceOfBook']");

    name.onfocus = function () {
        let nameSpan1 = document.querySelector("#nameSpan1");
        nameSpan1.innerText = "";
    };

    price.onfocus = function () {
        let priceSpan1 = document.querySelector("#priceSpan1");
        priceSpan1.innerText = "";
    };
};
var ret1, ret2;

function posalji1(form) {
    var name = form["NameOfBook"].value;
    var price = form["PriceOfBook"].value;

    if (!name) {
        let nameSpan1 = document.querySelector("#nameSpan1");
        nameSpan1.innerText = "polje ne sme biti prazno";
        ret1 = false;
    }

    else if (name.length >= 10) {
        let nameSpan1 = document.querySelector("#nameSpan1");
        nameSpan1.innerText = "dužina imena ne sme preći 10 karaktera";
        ret1 = false;
    }
    else { ret1 = true; }
  
    if (!price) {
        let priceSpan1 = document.querySelector("#priceSpan1");
        priceSpan1.innerText = "polje ne sme biti prazno";
        ret2 = false;
    }
  
    else if (isNaN(price)) {
        let priceSpan1 = document.querySelector("#priceSpan1");
        priceSpan1.innerText = "polje mora biti broj";
        ret2 = false;
    }

    else if (price < 0) {
        let priceSpan1 = document.querySelector("#priceSpan1");
        priceSpan1.innerText = "broj mora biti veci od nule";
        ret2 = false;
    }
    else { ret2 = true; }

    return ret1 && ret2;
}
